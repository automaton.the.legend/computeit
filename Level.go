package main

//Directions
const (
	Right = iota
	Down
	Left
	Up
)

type Level struct {
	Tiles [][]Tile
}

type Wire struct {
	Connections [4]bool
}

type Character struct {

}

type Tile struct {
	High interface{}
	Low interface{}
}